package lambda;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class CoffeeJsonUtil {

    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();

    public static Coffee fromJson(String jsonCoffee) {
        return GSON.fromJson(jsonCoffee, Coffee.class);
    }

    public static String toJson(Coffee coffee) {
        return GSON.toJson(coffee);
    }
}
