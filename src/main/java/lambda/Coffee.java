package lambda;

public record Coffee(String blend, String description) {}