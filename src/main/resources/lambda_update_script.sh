
awslocal lambda update-function-code --function-name get-coffee \
         --zip-file fileb://target/lambda.jar \
         --region us-east-1

awslocal lambda update-function-code --function-name create-coffee \
         --zip-file fileb://target/lambda.jar \
         --region us-east-1